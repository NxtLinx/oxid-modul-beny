# Oxid-Modul-Beny



## Getting started



## Add your files to the Oxid Shop

Copy this Modul in your module folder from the Oxid Shop
/source/modules/oe/
```
git clone https://gitlab.com/NxtLinx/oxid-modul-beny.git oebeny

vendor/bin/oe-console oe:module:install-configuration ./source/modules/oe/oebeny

cd <shop_directory>

composer config repositories.oxid-esales/beny-module path ./source/modules/oe/oebeny
composer require oxid-esales/beny-module:*

vendor/bin/oe-console oe:module:activate oebeny

```

