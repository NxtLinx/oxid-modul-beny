<?php

$aLang = array(
    'charset' => 'UTF-8',
    'SHOP_MODULE_GROUP_oebenyapikey' => 'Einstellungen Beny Zugang',
    'SHOP_MODULE_oebenyapi' => 'API-Key eintragen',
    'SHOP_MODULE_GROUP_oebenymarketplaces' => 'Einstellungen Marktplätze',
    'SHOP_MODULE_oebenymarketplace' => 'Hier die Marktplätze eintragen.'
    'SHOP_MODULE_GROUP_oebenymail' => 'Benachrichtigungsmail',
    'SHOP_MODULE_oebenymail' => 'Hier Ihre Beny Mail eintragen',
    'SHOP_MODULE_GROUP_oebenykeepold' => 'Einstellungen API Beny',
    'SHOP_MODULE_oebenykeepold' => 'Alte Artikel behalten',
    'SHOP_MODULE_GROUP_oebenycleanold' => 'Einstellungen API Beny',
    'SHOP_MODULE_oebenycleanold' => 'Alte Artikel löschen',
    'SHOP_MODULE_GROUP_oebenymaxandminprice' => 'Einstellungen API Beny',
    'SHOP_MODULE_oebenymaxandminprice' => 'Prozentangabe Min- und MaxPreis',
);

?>