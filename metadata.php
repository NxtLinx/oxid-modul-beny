

<?php

$sMetadataVersion = '1.0';


$aModule = array(
    'id'           => 'oebeny',
    'title'        => 'Beny Repricing',
    'description'  => array(
        'de' => 'Modul für die Preisanpassung.',
        'en' => 'Module for repricing.',
    ),
    'thumbnail'    => 'beny-logo.png',
    'version'      => '4.0.0',
    'author'       => 'OXID eSales AG',
    'url'          => 'https://www.beny-repricing.de/',
    'email'        => 'support@beny-repricing.com',
    'extend'       => array(
        
    ),
    'files' => array(
        // Core

        // Controllers
       
        // Admin

        // Models
     
    ),
    'settings' => array(
        
        array('group' => 'oebeny', 'name' => 'oebenyapi',      'type' => 'str',   'value' => ''),
        array('group' => 'oebeny', 'name' => 'oebenymarketplace',      'type' => 'str',   'value' => ''),
        array('group' => 'oebeny', 'name' => 'oebenymail',       'type' => 'str',   'value' => ''),
        array('group' => 'oebeny', 'name' => 'oebenykeepold', 'type' => 'bool',   'value' => 'false'),
        array('group' => 'oebeny', 'name' => 'oebenycleanold', 'type' => 'bool',   'value' => 'true'),
        array('group' => 'oebeny', 'name' => 'oebenymaxandminprice', 'type' => 'bool',   'value' => 'false'),

    )
);
