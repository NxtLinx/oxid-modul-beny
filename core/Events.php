<?php

public static function onActivate(): void
{
    $container = (new ContainerBuilderFactory())->create()->getContainer();
    $container->compile();

    $myService = $container->get('<service-id>');
    $myService->doSomething();
}

?>